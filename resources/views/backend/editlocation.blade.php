@extends('backend.master')

@section('content')

  <style>
  h1{
    text-align: center;
  };
  
</style>

<!-- Button trigger modal -->

<h1>Edit Locations</h1> 
<br><br>

<div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Location</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('editlocationprocess', $locations->id)}}" method="POST">
      @csrf
           <div class="modal-body">
        <div class="modal-body">
          <div class="form-group">
            <label for="formGroupExampleInput">Location Name</label>
            <input type="text" class="form-control" name="name" id="formGroupExampleInput" placeholder="Location Name" value="{{$locations->name}}">
          </div>
          <div class="form-group">
            <label for="formGroupExampleInput">Status</label>
            <select name="status" class="form-control" id="formGroupExampleInput">
              <option value="Active">Active</option>
              <option value="Inactive">Inactive</option>
            </select>
          </div>
          <div> 
          </div>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
@stop

@section('script')
  <script>
    $(document).ready(function(){
      $('#orderTable').DataTable();
    });

  </script>
@endsection