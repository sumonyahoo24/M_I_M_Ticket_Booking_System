@extends('backend.master')

@section('content')
<h1>User Details</h1> 
<br><br>         
 <div class="table-responsive" >
                    <table class="table table-striped" id="orderTable">
                      <thead>
                        <tr>
                          <th>Serial</th>
                          <th>Name</th>
                          <th>Address</th>
                          <th>Contact No</th>
                          <th></th>                                                   
                       </tr>
                      </thead>                      
                      <tbody>
                             @foreach($user as $key=>$user)
                          <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->address}}</td>
                            <td>{{$user->contact_num}}</td>
                          </tr>
                            @endforeach
                      </tbody>
                    </table>
</div>
@endsection