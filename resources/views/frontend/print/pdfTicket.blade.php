<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<table class="table table-bordered">
   
    <tbody>
        <tr>
        <td>Name: {{$downloadPdf->user->name}}</td>
        <td>Phone: {{$downloadPdf->user->contact_num}}</td>
    </tr>
    <tr>
        <td>From: {{$downloadPdf->coach->routelocation->from}}</td>
        <td>To: {{$downloadPdf->coach->routelocation->to}}</td>
    </tr>
    <tr>
        <td>Coach Number: {{$downloadPdf->coach_id}}</td>
        <td>Price: {{$downloadPdf->booking->unit_price}}</td>
    </tr>
    <tr>
        <td>Quantity: {{$downloadPdf->booking->quantity}}</td>
        <td>Total: {{$downloadPdf->booking->total_amount}}</td>
    </tr>
    <tr>
        @php
        $seats=json_decode($downloadPdf->seat);

        @endphp
        <td colspan="2" width="60%">Seat No:
        @foreach($seats as $seat)

            <p>{{$seat}}</p>
            
        @endforeach
        </td>
    </tr>
    
    </tbody>
    
</table>
</body>
</html>
