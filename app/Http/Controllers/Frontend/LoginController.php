<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validate;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Auth\User as Authenticatable;

class LoginController extends Controller
{
    public function login ()
    {
    	return view ('frontend.login');
    }

    public function dologin(Request $request)
    {
    	//dd($request->all());
    	// $request->validate([
     //     'email'=>'required',
     //     'password'=>'required',
    	// ]);
    	$credentials = [
    		'email' => $request->input('email'), 
    		'password' => $request->input('password'),
    	];
   	// dd($credentials);
    	if(Auth()->attempt($credentials))
    	{
            $user = User::where('email', ($request->input('email')))->first();
            //dd($user);
            if($user->role == "admin")
            {
                return redirect()->route('dashboard');
            }
            else
            {
               return redirect()->route('home')->with('message','Login Successfull');  
            }
    		
    		//return redirect()->route('home');
    	}
    	else
    	{
    		return redirect()->back();
    	}
    }
    //logout portion
    public function dologout()
    {
    	Auth::logout();
    	return view ('frontend.layouts.home');
    }
}
