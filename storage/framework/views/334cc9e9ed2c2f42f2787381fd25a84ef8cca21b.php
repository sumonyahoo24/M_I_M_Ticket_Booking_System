<?php $__env->startSection('content'); ?>
<style>
  .trip-list {
    color: #333;
  }
  .single-trip {
    display: flex;
    border: 1px solid #c5c5c5;
    align-items: center;
  }

  .single-trip {
    margin-bottom: 15px;
  }
  .trip-section {
    padding: 10px;
    flex: 1;
  }

span.st_num {
    padding: 2px;
    background: #87d60b;
    margin-left: 4px;
    color: red;
}
</style>
<br>

<section>
<div class="container">

<div class="trip-list">
  <div class="single-trip">
    <div class="trip_info d-flex">
      <div class="trip-section">
        Name:  <?php echo e(auth()->user()->name); ?>

      </div>
      <div class="trip-section">
        Coach Id::  <?php echo e($buses->coach_id); ?>

      </div>

      <div class="trip-section">From:: <?php echo e($buses->routelocation->from); ?>

      </div>
      <div class="trip-section">To:: <?php echo e($buses->routelocation->to); ?>

      </div>
      <div class="trip-section">Reporting:: <?php echo e($buses->reporting); ?>

      </div>
      <div class="trip-section">Depature:: <?php echo e($buses->depature); ?>

      </div>
      <div class="trip-section">Boarding:: <?php echo e($buses->boarding); ?>

      </div>
      <div class="trip-section">Seat Number:: 
        <?php $__currentLoopData = $seatNumber; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <span class="st_num">
            <?php echo e($data); ?>

          </span>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </div>
      <div class="trip-section">
        Unit Price::  <?php echo e($buses->price); ?>

      </div>
      <div class="trip-section">
        Total Price:: <?php echo e($totalPrice); ?>

      </div>
    </div>
    <div class="trip_amm">
      <form action="<?php echo e(route('ticket')); ?>" method="post" role="form">   
      <?php echo csrf_field(); ?>     
        <div class="form-group">
          <label for="bkash">Bkash (01711-711811)</label>
          <input type="text" class="form-control" id="bkash" name="bkash" placeholder="Enter bkash transaction id" required>
        </div>
        <div class="trip-section">          
          <button type="submit" class="btn btn-success">Confirm Booking</button>
        </div>
      </form>
    </div>
  </div>
</div>



</div>
</section><br>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\localhost\booking_system\resources\views/frontend/layouts/info.blade.php ENDPATH**/ ?>