<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<table class="table table-bordered">
   
    <tbody>
        <tr>
        <td>Name: <?php echo e($downloadPdf->user->name); ?></td>
        <td>Phone: <?php echo e($downloadPdf->user->contact_num); ?></td>
    </tr>
    <tr>
        <td>From: <?php echo e($downloadPdf->coach->routelocation->from); ?></td>
        <td>To: <?php echo e($downloadPdf->coach->routelocation->to); ?></td>
    </tr>
    <tr>
        <td>Coach Number: <?php echo e($downloadPdf->coach_id); ?></td>
        <td>Price: <?php echo e($downloadPdf->booking->unit_price); ?></td>
    </tr>
    <tr>
        <td>Quantity: <?php echo e($downloadPdf->booking->quantity); ?></td>
        <td>Total: <?php echo e($downloadPdf->booking->total_amount); ?></td>
    </tr>
    <tr>
        <?php
        $seats=json_decode($downloadPdf->seat);

        ?>
        <td colspan="2" width="60%">Seat No:
        <?php $__currentLoopData = $seats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $seat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <p><?php echo e($seat); ?></p>
            
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </td>
    </tr>
    
    </tbody>
    
</table>
</body>
</html>
<?php /**PATH E:\localhost\booking_system\resources\views/frontend/print/pdfTicket.blade.php ENDPATH**/ ?>